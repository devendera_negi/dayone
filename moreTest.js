const https = require("https");
const fs = require("fs");

const options = {
	hostname: "en.wikipedia.org",
	port: 443,
	path: "/wiki/Bangalore",
	method: "GET"
};

const req = https.request(options, function(res) {

	let responseBody = "";

	console.log("Response from wikipedia.");
	console.log(`Server Status: ${res.statusCode} `);
	console.log("Response Headers: %j", res.headers);

    // input stream will be a string
	res.setEncoding("UTF-8");

	res.once("data", function(chunk) {
		console.log('once:-', chunk);
	});

	res.on("data", function(chunk) {
		console.log(`--chunk-- ${chunk.length}`);
		responseBody += chunk;
	});

	res.on("end", function() {
		fs.writeFile("Bangalore.html", responseBody, function(err) {
			if (err) {
				throw err;
			}
			console.log("File Downloaded");
		});
	});

});

req.on("error", function(err) {
	console.log(`There is some problme : ${err.message}`);
});

req.end();
